#!/bin/usr/env python3
import sys
if len(sys.argv) < 2:
    quit()
number = sys.argv[1]
checkdigit = int(number[-1])
number = number[:-1]
total = checkdigit
i = 0
for n in number:
    n = int(n)
    if i % 2 == 0:
        i += 1
        total += n
        continue
    i += 1
    if n*2 > 9:
        total += n*2-9
    else:
        total += n*2

if total % 10 == 0:
    print("Valid")
else:
    print("Invalid"+str(total % 10))
